<li class="treeview">
          <a href="#">
            <!-- indiquer ici quel icone affiché -->
            <!-- <i class="fa {{ icon }}"></i> -->
            <span> {{ title }} </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url }}"><i class="fa fa-circle-o"></i> { title }</a></li>
            
            <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="../charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
</li>