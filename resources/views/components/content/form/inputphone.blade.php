<div class="form-group row">
    <label class="col-sm-2 col-form-label">{{ $title }}</label>

    <div class="input-group col-sm-10">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "+(99) 9 99 99 99 99"'>
                </div>
    <!-- /.input group -->
</div>