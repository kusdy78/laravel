<div class="form-group row">
    <label class="col-form-label">{{ $title }}:</label>

    <div class="input-group date" data-provide="datepicker">
        <input id="{{ $name }}" type="text" class="form-control col-sm-1">
        <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
        </div>
    </div>

    <!-- /.input group -->
</div>